export const getFormatedDate = (d = "", hideTime = false) => {
  if (!d) return "";
  let date = new Date(getStringDate(d));

  const options = { year: "numeric", month: "2-digit", day: "2-digit" };

  if (hideTime) return date.toLocaleDateString("en-US", options);
  return date.toLocaleString("en-US", {
    ...options,
    hour: "2-digit",
    minute: "2-digit",
    second: "2-digit"
  });
};

export const getFormatedDateFromDateString = (d = "", hideTime = false) => {
  d += " 14:00:00";
  return getFormatedDate(d, hideTime);
};

export const getTime = (d = "") => {
  const date = new Date(getStringDate(d));
  return date.getTime();
};

export const getStringDate = (d = "") => {
  let ua = navigator.userAgent.toLowerCase();
  let stringDate = d;
  const isUa = /firefox/i.test(ua);
  if (!isUa) stringDate = d.replace(/-/g, "/");
  stringDate += "+0000";
  return stringDate;
};

export const getDateFromTime = time => {
  const date = new Date(time);
  let month = date.getMonth();
  let day = date.getDate();
  month++;
  if (month < 10) month = "0" + month;
  if (day < 10) day = "0" + day;
  return `${date.getUTCFullYear()}-${month}-${day}`;
};
