import axios from "axios";

const PublisherService = {
  getAllPublishers: async () => {
    // const res = await HttpRequest.get("publisher/all");
    const res = await axios.get("assets/mocks/publishers.json");
    return res.data;
  }
};

export default PublisherService;
