import axios from "axios";

import HttpConfig from "config/HttpConfig";

class HttpRequest {
  static api = axios.create(HttpConfig);

  static get(url, config = {}) {
    return HttpRequest.api.get(url, {
      data: config.data || {},
      ...config
    });
  }

  static patch(url, data) {
    return HttpRequest.api.patch(url, data);
  }

  static post(url, data = {}) {
    return HttpRequest.api.post(url, data);
  }

  static put(url, data) {
    return HttpRequest.api.put(url, data);
  }

  static delete(url, config) {
    return HttpRequest.api.delete(url, config);
  }
}

export default HttpRequest;
