import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";

import SalesPage from "./views/sales";

import "./styles/index.scss";

function App() {
  return (
    <div className="app">
      <Router>
        <Switch>
          <Route path="/sales">
            <SalesPage />
          </Route>
          <Route path="/test" render={() => "test"}></Route>

          <Route
            path="/"
            render={() => <Redirect to={{ pathname: "sales" }} />}
          />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
