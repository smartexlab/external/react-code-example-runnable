import React from "react";
import { NavLink } from "react-router-dom";

import { ReactComponent as FooterBgIcon } from "assets/footer-bg-ic.svg";
import { ReactComponent as FooterLogo } from "assets/footer-logo.svg";

import "./pageFooter.scss";

function PageFooter() {
  return (
    <div className="page-footer position-relative">
      <div className="footer-bg">
        <FooterBgIcon />
      </div>
      <div className="wrapper">
        <div className="footer-logo">
          <FooterLogo />
        </div>
        <div className="footer-line"></div>
        <div className="justify-content-center d-flex mt-50px">
          <NavLink to="/terms">
            <div className="text-white mr-50px text-small font-csb ">
              Term & Conditions
            </div>
          </NavLink>
          <NavLink to="/privacy">
            <div className="text-white mr-50px text-small font-csb ">
              Privacy Policy
            </div>
          </NavLink>
          <NavLink to="/contact">
            <div className="text-white mr-50px text-small font-csb ">
              Contact
            </div>
          </NavLink>
          <NavLink to="/help">
            <div className="text-white mr-50px text-small font-csb ">Help</div>
          </NavLink>
        </div>
        <div className="copyright">&copy; 2019 nowtrade all right reserved</div>
      </div>
    </div>
  );
}

export default PageFooter;
