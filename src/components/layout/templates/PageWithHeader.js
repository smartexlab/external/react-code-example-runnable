import React from "react";
import PageHeader from "../PageHeader";

import "./pageWithHeader.scss";
import PageFooter from "../PageFooter";

function PageWithHeader(props) {
  return (
    <div className="page-with-header">
      <div className="content-container-outer content-container-inner">
        <PageHeader dark={props.dark} />
      </div>
      <div className="content">{props.children}</div>
      <div>
        <PageFooter />
      </div>
    </div>
  );
}

export default PageWithHeader;
