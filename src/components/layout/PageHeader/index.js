import React from "react";
import { NavLink } from "react-router-dom";

import { ReactComponent as Logo } from "../../../assets/nav-logo.svg";

import "./pageHeader.scss";

function PageHeader({ dark }) {
  const darkClass = dark ? "dark-links" : "";

  return (
    <div className="page-header d-flex align-items-center">
      <div className="mr-auto">
        <NavLink to="/sales">
          <Logo />
        </NavLink>
      </div>
      <div className={`d-flex align-items-center links ${darkClass}`}>
        <NavLink to="/alerts" className="h5 mr-40px">
          Trade Alerts
        </NavLink>
        <NavLink to="/traders" className="h5 mr-40px">
          Traders
        </NavLink>
        <NavLink to="/sign-up" className="h5 mr-40px">
          Sign up
        </NavLink>
        <NavLink to="/login">
          <button className="btn-white">Login</button>
        </NavLink>
      </div>
    </div>
  );
}

export default PageHeader;
