import React from "react";

import { AlertStatus } from "constants/alert";
import { getFormatedDate } from "services/utils/date";

function TraderBlock(props) {
  const alert = props.alert;
  return (
    <div className="trader-block py-4">
      <div className="d-flex">
        <div className="avatar-medium mr-32px">
          <img src={alert.publisher.avatar.small} alt="" />
        </div>
        <div>
          <h4 className="text-white font-csbold">{alert.publisher.name}</h4>
          <div className="text-white text-small font-csm">
            {getFormatedDate(alert.createdAt)}
          </div>
          <div
            className={
              "status-block text-small font-csb mt-4 status-" +
              alert.alertStatusId
            }
          >
            {AlertStatus[alert.alertStatusId]}
          </div>
        </div>
      </div>
    </div>
  );
}

export default TraderBlock;
