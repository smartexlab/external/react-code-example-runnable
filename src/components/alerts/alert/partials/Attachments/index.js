import React, { useState } from "react";
import FsLightbox from "fslightbox-react";

import Attachment from "./partials/Attachment";

import "./attachments.scss";

function Attachments(props) {
  const [lightboxController, setLightboxController] = useState({
    toggler: false,
    slide: 1
  });

  function openLightboxOnSlide(number) {
    setLightboxController({
      toggler: !lightboxController.toggler,
      slide: number
    });
  }

  const attachments = props.attachments;
  if (!attachments.length) return null;

  const firstAuxAttachment = attachments[1];
  const secondAuxAttachment = attachments[2];
  const sources = attachments.map(
    attachment => attachment.embedCode || attachment.url
  );

  return (
    <div className="alert-attachments">
      <div className="main-attachment" onClick={() => openLightboxOnSlide(1)}>
        <Attachment attachment={attachments[0]} />
      </div>
      <div className="aux-attachments d-flex mt-1">
        {firstAuxAttachment ? (
          <div className="w-50" onClick={() => openLightboxOnSlide(2)}>
            <Attachment attachment={firstAuxAttachment} />
          </div>
        ) : null}
        {secondAuxAttachment ? (
          <div className="w-50" onClick={() => openLightboxOnSlide(3)}>
            <Attachment attachment={secondAuxAttachment} />
          </div>
        ) : null}
      </div>

      <FsLightbox
        toggler={lightboxController.toggler}
        sources={sources}
        slide={lightboxController.slide}
      />
    </div>
  );
}

export default Attachments;
