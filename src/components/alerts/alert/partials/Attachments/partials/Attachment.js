import React from "react";

import { AlertAttachment } from "constants/alert";

function Attachment(props) {
  const attachment = props.attachment;

  const attachmentBlock =
    attachment.alertAttachmentTypeId === AlertAttachment.Img ? (
      <img src={attachment.url} alt={`img-${attachment.id}`} />
    ) : (
      <video
        data-src={attachment.embedCode}
        src={attachment.embedCode}
        controls
        controlsList="nodownload"
      />
    );

  return <div className="attachment">{attachmentBlock}</div>;
}

export default Attachment;
