import React from "react";
import { AlertDurability } from "constants/alert";

function InfoBlock(props) {
  const alert = props.alert;
  return (
    <div className="info-block position-relative pl-4 py-4">
      <div className="d-flex align-items-center">
        <h3 className="text-white font-csm mr-50px">
          {AlertDurability[alert.alertDurabilityId]}
        </h3>

        <div className="currency-block text-small font-csm">
          {alert.currency.abbreviation} - {alert.currency.name}
        </div>
      </div>

      <div className="d-flex align-items-center mt-40px text-white">
        <div className="d-flex mr-5 align-items-center">
          <div>Entry</div>
          <div className="num-block">{alert.entry}</div>
        </div>

        <div className="d-flex mr-5 align-items-center">
          <div>Target</div>
          <div className="num-block">{alert.target}</div>
        </div>

        <div className="d-flex align-items-center">
          <div>Stop Loss</div>
          <div className="num-block">{alert.stopLoss}</div>
        </div>
      </div>
    </div>
  );
}

export default InfoBlock;
