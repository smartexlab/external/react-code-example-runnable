import React from "react";
import TraderBlock from "./partials/TraderBlock";

import "./alert.scss";
import InfoBlock from "./partials/InfoBlock";
import Attachments from "./partials/Attachments";

function Alert(props) {
  return (
    <div className="alert-block position-relative">
      <div className="d-flex px-5 header-border">
        <div className="mr-50px">
          <TraderBlock alert={props.alert} />
        </div>
        <div className="info-border">
          <InfoBlock alert={props.alert} />
        </div>
      </div>

      <div className="mx-1">
        <Attachments attachments={props.alert.attachments} />
        <div className="alert-content text-white">{props.alert.content}</div>
      </div>
    </div>
  );
}

export default Alert;
