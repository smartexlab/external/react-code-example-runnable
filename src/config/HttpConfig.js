const HttpConfig = {
  baseURL: "https://api.url",
  responseType: "json",
  headers: {
    "Content-Type": "application/json"
  },
  data: {}
};

export const frontURL = "localhost:3000";

export default HttpConfig;
