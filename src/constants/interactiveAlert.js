export const interactiveAlert = {
  publisher: {
    name: "CtheLightTrading",
    avatar: {
      small: "assets/sales/traderAvatar.jpg",
      medium: "assets/sales/tradeAvater.jpg"
    }
  },
  alertStatusId: 2,
  alertDurabilityId: 2,
  currency: { id: 2, name: "Trade Desk Inc", abbreviation: "TTD" },
  entry: 140,
  target: 202,
  stopLoss: 132,
  createdAt: "2019-01-28 14:03:00",
  content: `During the latter part of 2018 we identified a fast growing Company with a solid technical set up that provided a compelling Risk Reward.\n\nAs we violated the upside Breakout area of the Trendline the week of 1/28/2019 and after an ABC flat type correction  we entered Long @ 140.00 common stock and or Call options using a Stop loss of 132.00 with Target 1 of 198-202 and Target 2 255-260`,
  attachments: [
    {
      alertAttachmentTypeId: 1,
      url: "assets/sales/sampleTrade.png"
    }
  ],
  updates: []
};
