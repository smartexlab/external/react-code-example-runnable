export const AlertStatus = {
  1: "Open",
  2: "Close"
};

export const AlertDurability = {
  1: "Short",
  2: "Long"
};

export const AlertAttachment = {
  Img: 1,
  Video: 2
};
