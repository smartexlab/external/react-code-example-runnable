import React from "react";

import { ReactComponent as TopTradersIc } from "assets/home/top-traders-ic.svg";
import { ReactComponent as SimpleIC } from "assets/home/simple-ic.svg";
import { ReactComponent as EasyIc } from "assets/home/easy-ic.svg";

import "./salesWhy.scss";

function SalesWhy(props) {
  return (
    <div className="sales-why">
      <div className="content-container-outer content-container-inner position-relative left-border">
        <div className="d-flex justify-content-center align-items-center pt-5">
          <div className="sales-line mr-2 mt-1"></div>
          <div className="text-red-light font-csbold">Why</div>
        </div>
        <h1 className="text-center mt-4">Why Should I Try NowTrade?</h1>

        <div className="row why-blocks">
          <div className="col-md-4">
            <div className="why-block selected">
              <div className="block-inner">
                <TopTradersIc />
                <h5 className="text-white font-csb mt-40px">Top Traders</h5>
                <div className="block-line mt-20px"></div>
                <div className="text-white text-small mt-40px font-mr">
                  {props.tradersCount} Professional traders who you already know
                  and can feel comfortable with.
                </div>
              </div>
            </div>
          </div>

          <div className="col-md-4">
            <div className="why-block">
              <div className="block-inner">
                <SimpleIC />
                <h5 className="font-csb mt-40px">Simple</h5>
                <div className="block-line mt-20px"></div>
                <div className="text-small mt-40px font-mr">
                  The most selective idea from each trader per month.
                </div>
              </div>
            </div>
          </div>

          <div className="col-md-4">
            <div className="why-block">
              <div className="block-inner">
                <EasyIc />
                <h5 className="font-csb mt-40px">Easy</h5>
                <div className="block-line mt-20px"></div>
                <div className="text-small mt-40px font-mr">
                  Have your hand held - consistent detailed commentary from
                  entry to exit.
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SalesWhy;
