import React from "react";

import "./salesResults.scss";

function SalesResults() {
  return (
    <div className="sales-results position-relative">
      <div className="row">
        <div className="col-6 results-image">
          <img src="assets/sales/results.png" alt=""></img>
        </div>
        <div className="col-6 results-content d-flex justify-content-center align-items-center">
          <div className="content-inner">
            <div className="d-flex align-items-center">
              <div className="sales-line mr-2"></div>
              <div className="text-red-light font-csbold">Get Results</div>
            </div>

            <h1 className="mt-32px">Insight to Become a Better Trader</h1>

            <div className="mt-32px">
              <div className="h4 text-black d-flex align-items-center">
                <div className="text-red-light mr-4">x</div>
                <div className="text-black">No Education</div>
              </div>
              <div className="h4 text-black d-flex align-items-center">
                <div className="text-red-light mr-4">x</div>
                <div className="text-black">No Report</div>
              </div>
              <div className="h4 text-black d-flex align-items-center">
                <div className="text-red-light mr-4">x</div>
                <div className="text-black">No Analsis</div>
              </div>
            </div>

            <h4 className="text-uppercase text-black mt-50px">
              Actionable Setups Only
            </h4>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SalesResults;
