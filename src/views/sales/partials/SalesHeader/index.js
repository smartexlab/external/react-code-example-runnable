import React from "react";
import { NavLink } from "react-router-dom";

import { ReactComponent as HomeHeroBg } from "assets/home/home-hero-bg.svg";
import { ReactComponent as HomeHeroIl } from "assets/home/home-hero-il.svg";

import "./salesHeader.scss";

function SalesHeader() {
  return (
    <div className="sales-header position-relative">
      <div className="backgrounds">
        <div className="hero-bg">
          <HomeHeroBg />
        </div>
        <div className="hero-il">
          <HomeHeroIl />
        </div>
        {/* <div className="shape top-shape"></div> */}
      </div>

      <div className="position-relative content-container-outer content-container-inner left-border sales-header-content">
        <h1>
          <div>The Best Trade Idea</div>
          <div>Every Month</div>
        </h1>

        <h4 className="mt-4">
          <div>Become a better trader by leveraging</div>
          <div>the experience of proven traders.</div>
        </h4>

        <button className="btn-rect btn-blue h5 font-csb mt-50px">
          Become a Member
        </button>

        <div className="mt-32px">
          <NavLink
            to="/sign-up"
            className="h5 font-msb text-blue hover-text-blue"
          >
            Only 10$ per month.
          </NavLink>
        </div>
      </div>
    </div>
  );
}

export default SalesHeader;
