import React, { useState, useEffect } from "react";
import Glide from "@glidejs/glide";

import "@glidejs/glide/src/assets/sass/glide.core.scss";

import "./salesTraders.scss";
import { formatLinks } from "services/utils/format";
import glideConfig from "./glideConfig";

function SalesTraders(props) {
  let [glide, setGlide] = useState({});
  let [currentSlide, setCurrentSlide] = useState(-1);

  useEffect(() => {
    let glide = new Glide(".glide", glideConfig);

    glide.mount();

    setGlide(glide);
  }, []);

  const moveToSlide = index => {
    glide.go(`=${index}`);
    setCurrentSlide(index);
  };

  let timer;
  const onHover = index => ev => {
    if (index === currentSlide) return;
    if (timer) clearTimeout(timer);
    timer = setTimeout(() => moveToSlide(index), 500);
  };

  const traders = props.traders;
  const cards = traders.map((trader, index) => {
    return (
      <li
        className="glide__slide"
        key={`trader-${trader.id}`}
        onClick={() => moveToSlide(index)}
        onMouseOver={onHover(index)}
        onMouseOut={() => clearTimeout(timer)}
      >
        <div className="trader-photo">
          <img
            className="avatar-medium"
            src={`${trader.avatar.medium}?${Date.now()}`}
            alt=""
          ></img>
        </div>

        <div className="hidden-part">
          <h5 className="text-center text-black font-csm">{trader.name}</h5>
          <div
            className="trader-bio text-small mt-3"
            dangerouslySetInnerHTML={{ __html: formatLinks(trader.bio) }}
          ></div>
          <div className="trader-twitter d-flex justify-content-center align-items-center mt-3">
            <a
              href={`https://twitter.com/${trader.twitterTag}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <i className="fab fa-twitter text-white"></i>
            </a>
          </div>
        </div>
      </li>
    );
  });

  return (
    <div className="sales-traders">
      <div className="content-container-outer content-container-inner position-relative left-border">
        <div className="trades-howit mx-auto">
          <div className="d-flex justify-content-center align-items-center pt-5">
            <div className="sales-line mr-2 mt-1"></div>
            <div className="text-red-light font-csbold">Traders</div>
          </div>
          <h1 className="text-center mt-4">Your Featured Traders</h1>
          <div className="mt-20px text-center">
            {traders.length} independent traders that you know and can trust
            provide their best trade idea every month.
          </div>
        </div>

        <div className="glide mt-50px">
          <div className="glide__track" data-glide-el="track">
            <ul className="glide__slides">{cards}</ul>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SalesTraders;
