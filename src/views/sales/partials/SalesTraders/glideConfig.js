export default {
  focusAt: "center",
  startAt: 2,
  perView: 5,
  breakpoints: {
    1600: {
      perView: 2,
      startAt: 1
    },
    1300: {
      perView: 3,
      startAt: 1
    },
    1280: {
      perView: 2,
      startAt: 1
    }
  }
};
