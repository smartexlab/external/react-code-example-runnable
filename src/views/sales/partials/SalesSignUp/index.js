import React from "react";
import { NavLink } from "react-router-dom";

import "./salesSignUp.scss";

function SalesSingUp() {
  return (
    <div className="sales-signup">
      <div className="content-container-outer content-container-inner">
        <div className="wrapper">
          <div className=" row">
            <div className="col-md-5">
              <h1 className="font-csbold mb-4">Sign Up Now</h1>
              <div>And access a wealth of trade knowledge on-demand.</div>
              <NavLink to="/sign-up">
                <button className="mt-50px blue-rect-button">
                  Become A Member
                </button>
              </NavLink>
            </div>
            <div className="col-md-7 card-price text-white">
              <h2>Monthly Membership: </h2>
              <h4 className="font-csb mt-32px text-white">
                Only $10/month. Zero
              </h4>
              <h4 className="font-csb text-white">commitment.</h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SalesSingUp;
