import React from "react";
import ReactDOM from "react-dom";
import ReactTooltip from "react-tooltip";

import "./interactiveTooltip.scss";

function InteractiveTooltip(props) {
  const tip = props.tip;

  const style = {};
  if (tip.top) style.top = tip.top;
  if (tip.left) style.left = tip.left;

  const tooltip = (
    <div
      data-tip
      data-for={`ineractive-tooltip-${tip.id}`}
      className="interactive-tooltip"
      style={style}
    >
      <div className="interactive-tooltip-inner">
        <div className="position-relative">
          <ReactTooltip
            className="tooltip-block"
            id={`ineractive-tooltip-${tip.id}`}
          >
            {tip.text}
          </ReactTooltip>
        </div>
      </div>
    </div>
  );

  return ReactDOM.createPortal(
    tooltip,
    document.getElementsByClassName(tip.elementClassName)[0]
  );
}

export default InteractiveTooltip;
