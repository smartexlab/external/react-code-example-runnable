import React, { useState, useEffect } from "react";

import Alert from "components/alerts/alert";

import "./salesTrades.scss";
import { interactiveAlert } from "constants/interactiveAlert";
import InteractiveTooltip from "./partial/ineractiveTooltip";

function SalesTrades(props) {
  const [rendered, setRendered] = useState(false);

  useEffect(() => {
    setRendered(true);
  }, []);

  return (
    <div className="sales-trades">
      <div className="backgrounds"></div>
      <div className="content-container-outer position-relative">
        <div className="content-container-inner">
          <div className="trades-howit mx-auto">
            <div className="d-flex justify-content-center align-items-center pt-5">
              <div className="sales-line mr-2 mt-1"></div>
              <div className="text-red-light font-csbold">How it Works</div>
            </div>
            <h1 className="text-center mt-4">How does NowTrade work?</h1>
            <div className="mt-20px text-center">
              {props.tradersCount} independent traders that you know and can
              trust provide their best trade idea every month. They'll keep you
              informed along the way with full commentary from entry to exit of
              the position. Take a test drive of what an alert feels like on
              NowTrade below.
            </div>
          </div>

          <div className="alert-wrapper ">
            <Alert alert={interactiveAlert} />

            {rendered ? (
              <InteractiveTooltip
                tip={{
                  id: 1,
                  text: "See how it Works!",
                  elementClassName: "trader-block",
                  top: "10px",
                  left: "10px"
                }}
              />
            ) : null}
          </div>
        </div>
      </div>
    </div>
  );
}

export default SalesTrades;
