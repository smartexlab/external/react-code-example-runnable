import React, { useState, useEffect } from "react";

import PageWithHeader from "../../components/layout/templates/PageWithHeader";
import PublisherService from "services/PublisherService";

import SalesHeader from "./partials/SalesHeader";
import SalesTrades from "./partials/SalesTrades";
import SalesTraders from "./partials/SalesTraders";

import "./sales.scss";
import SalesResults from "./partials/SalesResults";
import SalesWhy from "./partials/SalesWhy";
import SalesSingUp from "./partials/SalesSignUp";

function SalesPage() {
  let [loading, setLoading] = useState(true);
  let [traders, setTraders] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      const traders = await PublisherService.getAllPublishers();
      setTraders(traders);
      setLoading(false);
    };

    fetchData();
  }, []);

  return loading ? null : (
    <PageWithHeader dark>
      <div className="sales-page">
        <SalesHeader dark />
        <SalesTrades tradersCount={traders.length} />
        <SalesTraders traders={traders} />
        <SalesResults />
        <SalesWhy tradersCount={traders.length} />
        <SalesSingUp />
      </div>
    </PageWithHeader>
  );
}

export default SalesPage;
